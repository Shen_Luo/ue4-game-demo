// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameDemo.h"
#include "InteractableActor.generated.h"

UCLASS()
class GAMEDEMO_API AInteractableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AInteractableActor();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Interaction)
	void OnInteract(AActor* Caller);
	virtual void OnInteract_Implementation(AActor* Caller);

	void OnBeginFocus();
	void OnEndFocus();

	void GetPlayer(AActor* Caller);

protected:
	

private:
	class AMyPlayer* Player;

	UPROPERTY(EditDefaultsOnly)
	bool bCanInteract = false;

	TArray<class UMeshComponent*> Meshs;

	UPROPERTY(EditDefaultsOnly)
	EStencilColor Color = EStencilColor::SC_Blue;
};
