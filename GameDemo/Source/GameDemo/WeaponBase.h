// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PickUpBase.h"
#include "WeaponBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEDEMO_API AWeaponBase : public APickUpBase
{
	GENERATED_BODY()
	
public: 
	AWeaponBase();
	virtual void BeginPlay() override;

	//INTERACTIONS
	void AddAmmo(int32 Amount);
	void DealDamage(const FHitResult& Hit);
	virtual void DoFire();
	void StartFire();
	void StopFire();
	FVector CalcSpread();
	void SpawnFireEffect();
	void SpawnImpactEffect(const FHitResult& Hit);
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void ChangeOwner(AActor* NewOwner);
	UFUNCTION(BlueprintCallable, Category = Weapon)
		void GetAmmo(int32 &Current, int32 &Max);

	virtual void OnInteract_Implementation(AActor* Caller) override;
	bool HasFullAmmo() const;



protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bUseProjectile"))
		TSubclassOf<class AProjectileBase> ProjectileClass;
	UPROPERTY(EditDefaultsOnly)
		FName MuzzleSocketName;

	class AMyPlayer* OwningPlayer;
	bool bIsFiring;
	int32 CurrentAmmo;

private: 	
	UPROPERTY(EditDefaultsOnly)
		EAmmoType AmmoType;
	UPROPERTY(EditDefaultsOnly)
		int32 MaxAmmo;
	UPROPERTY(EditDefaultsOnly)
		float Spread;
	UPROPERTY(EditDefaultsOnly)
		float FireRate;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseProjectile"))
		float MaxRange;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseProjectile"))
		float BaseDamage;
	UPROPERTY(EditDefaultsOnly)
		bool bUseProjectile = false;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* ShotEffect;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseProjectile"))
		UParticleSystem* ImpactEffect;
	UPROPERTY(EditDefaultsOnly)
		USoundCue* ShotSound;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseProjectile"))
		USoundCue* ImpactSound;
	
	
	FCollisionQueryParams TraceParams;
	FTimerHandle FireRateHandle;
	
};
