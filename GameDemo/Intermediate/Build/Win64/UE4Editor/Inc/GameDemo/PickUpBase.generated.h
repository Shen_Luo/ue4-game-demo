// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAMEDEMO_PickUpBase_generated_h
#error "PickUpBase.generated.h already included, missing '#pragma once' in PickUpBase.h"
#endif
#define GAMEDEMO_PickUpBase_generated_h

#define GameDemo_Source_GameDemo_PickUpBase_h_12_RPC_WRAPPERS \
	virtual void OnInteract_Implementation(AActor* Caller); \
 \
	DECLARE_FUNCTION(execOnTriggerEnter) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnTriggerEnter(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnInteract) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Caller); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnInteract_Implementation(Z_Param_Caller); \
		P_NATIVE_END; \
	}


#define GameDemo_Source_GameDemo_PickUpBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnTriggerEnter) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnTriggerEnter(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnInteract) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Caller); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnInteract_Implementation(Z_Param_Caller); \
		P_NATIVE_END; \
	}


#define GameDemo_Source_GameDemo_PickUpBase_h_12_EVENT_PARMS \
	struct PickUpBase_eventOnInteract_Parms \
	{ \
		AActor* Caller; \
	};


extern GAMEDEMO_API  FName GAMEDEMO_OnInteract;
#define GameDemo_Source_GameDemo_PickUpBase_h_12_CALLBACK_WRAPPERS
#define GameDemo_Source_GameDemo_PickUpBase_h_12_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAPickUpBase(); \
	friend GAMEDEMO_API class UClass* Z_Construct_UClass_APickUpBase(); \
	public: \
	DECLARE_CLASS(APickUpBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GameDemo"), NO_API) \
	DECLARE_SERIALIZER(APickUpBase) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GameDemo_Source_GameDemo_PickUpBase_h_12_INCLASS \
	private: \
	static void StaticRegisterNativesAPickUpBase(); \
	friend GAMEDEMO_API class UClass* Z_Construct_UClass_APickUpBase(); \
	public: \
	DECLARE_CLASS(APickUpBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/GameDemo"), NO_API) \
	DECLARE_SERIALIZER(APickUpBase) \
	/** Indicates whether the class is compiled into the engine */ \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GameDemo_Source_GameDemo_PickUpBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickUpBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickUpBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUpBase); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API APickUpBase(const APickUpBase& InCopy); \
public:


#define GameDemo_Source_GameDemo_PickUpBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API APickUpBase(const APickUpBase& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickUpBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickUpBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickUpBase)


#define GameDemo_Source_GameDemo_PickUpBase_h_9_PROLOG \
	GameDemo_Source_GameDemo_PickUpBase_h_12_EVENT_PARMS


#define GameDemo_Source_GameDemo_PickUpBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameDemo_Source_GameDemo_PickUpBase_h_12_RPC_WRAPPERS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_CALLBACK_WRAPPERS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_INCLASS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GameDemo_Source_GameDemo_PickUpBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GameDemo_Source_GameDemo_PickUpBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_CALLBACK_WRAPPERS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_INCLASS_NO_PURE_DECLS \
	GameDemo_Source_GameDemo_PickUpBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GameDemo_Source_GameDemo_PickUpBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
