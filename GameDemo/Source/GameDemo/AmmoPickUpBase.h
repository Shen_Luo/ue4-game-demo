// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PickUpBase.h"
#include "AmmoPickUpBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEDEMO_API AAmmoPickUpBase : public APickUpBase
{
	GENERATED_BODY()
	
public:
	virtual void OnInteract_Implementation(AActor* Caller) override;

private:
	UPROPERTY(EditDefaultsOnly)
		int32 AmmoAmount;
	UPROPERTY(EditDefaultsOnly)
		EAmmoType AmmoType;


	
};
