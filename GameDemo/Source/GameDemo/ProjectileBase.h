// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ProjectileBase.generated.h"

UCLASS()
class GAMEDEMO_API AProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AProjectileBase();

	// OVERRIDES:
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaTime) override;

public:
	void Trace(float DeltaTime);
	void DealDamage(FHitResult& Hit);
	void SpawnImpactEffect(FHitResult& Hit);
	void Initialize(FVector Direction);
	void Move(float DeltaTime);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ProjectileBase, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* ProjectileMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ProjectileBase, meta = (AllowPrivateAccess = "true"))
		UParticleSystemComponent* ProjectileParticles;
	
private:

	UPROPERTY(EditDefaultsOnly)
		float InitialSpeed;
	UPROPERTY(EditDefaultsOnly)
		USoundCue* ImpactSound;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* ImpactEffect;
	UPROPERTY(EditDefaultsOnly)
	FRadialDamageParams RadDamageParams;
	

	FVector Velocity;
	FCollisionQueryParams TraceParams;
	FRadialDamageEvent RadDamageEvent;
	

};
