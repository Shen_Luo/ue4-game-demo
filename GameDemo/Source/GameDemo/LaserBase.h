// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WeaponBase.h"
#include "LaserBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEDEMO_API ALaserBase : public AWeaponBase
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditDefaultsOnly) //per second
	float RechargeAmount;
	float Charge;
	
};
