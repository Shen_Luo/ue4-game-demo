// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "MyPlayer.h"
#include "MyHealthPickUp.h"


void AMyHealthPickUp::OnInteract_Implementation(AActor* Caller)
{
	AMyPlayer* Player = Cast<AMyPlayer>(Caller);
	if (Player)
	{
		Player->Heal(HealAmount);
		Destroy();
	}
}

