// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "WeaponBase.h"
#include "MyPlayer.h"
#include "Engine.h"

AWeaponBase::AWeaponBase()
{
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));
	WeaponMesh->AttachTo(RootComponent);
	TraceParams = FCollisionQueryParams(FName("Projectile trace"), true, this);
	TraceParams.TraceTag = FName("HitscanTrace");
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = MaxAmmo;

	GetWorld()->DebugDrawTraceTag = FName("HitscanTrace");
}

void AWeaponBase::AddAmmo(int32 Amount)
{
	CurrentAmmo = FMath::Clamp(CurrentAmmo + Amount, CurrentAmmo, MaxAmmo);
}

void AWeaponBase::DealDamage(const FHitResult& Hit)
{
	float DealtDamage = BaseDamage;
	FVector ShotDir = GetActorLocation() - Hit.ImpactPoint;
	FPointDamageEvent DamageEvent;
	DamageEvent.Damage = DealtDamage;
	DamageEvent.HitInfo = Hit;
	DamageEvent.ShotDirection = ShotDir;
	DamageEvent.ShotDirection.Normalize();

	Hit.GetActor()->TakeDamage(DealtDamage, DamageEvent, OwningPlayer->GetController(), this);



}

void AWeaponBase::StartFire()
{
	bIsFiring = true;
	if (CurrentAmmo > 0)
	{
		DoFire();
		float TimeDelay = FireRate > 0 ? 1 / (FireRate*1/60) : FApp::GetDeltaTime();

		UWorld* World = GetWorld();
		if (World)
		{
			if (!FireRateHandle.IsValid())
			{
				World->GetTimerManager().SetTimer(FireRateHandle, this, &AWeaponBase::StartFire, TimeDelay, true);
			}
		}
		
	}
	else
	{
		StopFire();
	}
}

void AWeaponBase::StopFire()
{
	bIsFiring = false;
	UWorld* World = GetWorld();
	if (World)
	{
		World->GetTimerManager().ClearTimer(FireRateHandle);
		FireRateHandle.Invalidate();
	}
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Stop Fire!"));
}

void AWeaponBase::DoFire()
{
	FHitResult Hit(ForceInit);
	FVector Start = WeaponMesh->GetSocketLocation(MuzzleSocketName);
	FVector End = Start + CalcSpread() * MaxRange;

	UWorld* World = GetWorld();
	if (World)
	{
		World->LineTraceSingleByChannel(Hit, Start, End, ECC_GameTraceChannel1, TraceParams);
	}

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Fire!"));

	CurrentAmmo--;
	SpawnFireEffect();
	SpawnImpactEffect(Hit);
	if (Hit.GetActor())
	{
		DealDamage(Hit);
		UE_LOG(LogTemp, Warning, TEXT("Hit! %s"), *Hit.GetActor()->GetName());
	}
}

FVector AWeaponBase::CalcSpread()
{
	if (OwningPlayer)
	{
		FVector Direction = OwningPlayer->GetActorRotation().Vector();
		float Angle = atan(Spread / 10000);
		return FMath::VRandCone(Direction, Angle);		
	}
	else
	{
		FVector Direction = GetActorRotation().Vector();
		float Angle = atan(Spread / 10000);
		return FMath::VRandCone(Direction, Angle);
	}
}

void AWeaponBase::SpawnFireEffect()
{
	FVector Location = WeaponMesh->GetSocketLocation(MuzzleSocketName);
	FRotator Rotation = WeaponMesh->GetSocketRotation(MuzzleSocketName);
	UGameplayStatics::SpawnEmitterAttached(ShotEffect, WeaponMesh, MuzzleSocketName, Location, Rotation, EAttachLocation::KeepWorldPosition,true);
	UGameplayStatics::PlaySoundAttached(ShotSound, WeaponMesh, MuzzleSocketName, Location, EAttachLocation::KeepWorldPosition, true, 1, 1, 0);
}

void AWeaponBase::SpawnImpactEffect(const FHitResult& Hit)
{
	FVector Location = Hit.ImpactPoint;
	FRotator Rotation = Hit.ImpactNormal.Rotation();
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Location, Rotation, true);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Location, Rotation, 1, 1, 0);
}

void AWeaponBase::ChangeOwner(AActor* NewOwner)
{
	AMyPlayer* Player = Cast<AMyPlayer>(NewOwner);
	if (Player)
	{
		OwningPlayer = Player;
	}
	SetOwner(NewOwner);
}

void AWeaponBase::OnInteract_Implementation(AActor* Caller)
{
	AMyPlayer* Player = Cast<AMyPlayer>(Caller);
	if (Player)
	{
		Player->AddToInventory(this);
	}
}

void AWeaponBase::GetAmmo(int32 &Current, int32 &Max)
{
	Current = CurrentAmmo;
	Max = MaxAmmo;
}

bool AWeaponBase::HasFullAmmo() const
{
	return CurrentAmmo == MaxAmmo ? true : false;
}
