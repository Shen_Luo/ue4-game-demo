// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "AmmoPickUpBase.h"
#include "MyPlayer.h"

void AAmmoPickUpBase::OnInteract_Implementation(AActor* Caller)
{
	AMyPlayer* Player = Cast<AMyPlayer>(Caller);
	if (Player)
	{
		Player->AddAmmo(AmmoAmount, AmmoType);
	}
	Destroy();
}




