// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "ProjectileBase.h"


AProjectileBase::AProjectileBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	ProjectileParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile Particle System"));
	RootComponent = ProjectileMesh;
	ProjectileParticles->AttachTo(RootComponent);

	TraceParams = FCollisionQueryParams(FName("Projectile trace"), false, this);

}

void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AProjectileBase::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	Trace(DeltaTime);
	Move(DeltaTime);

}

void AProjectileBase::Trace(float DeltaTime)
{
	FVector Start = GetActorLocation();
	FVector End = Start + Velocity * DeltaTime;
	FHitResult Hit;
	GetWorld()->LineTraceSingleByChannel(Hit,Start, End, ECC_Visibility, TraceParams);
	if (Hit.GetActor())
	{
		SpawnImpactEffect(Hit);
		DealDamage(Hit);
		Destroy();
	}
}

void AProjectileBase::DealDamage(FHitResult& Hit)
{
	TArray<FHitResult> Hits;

	FVector Start = Hit.ImpactPoint;
	FVector End = Start + FVector(0, 0, 1);
	GetWorld()->SweepMultiByChannel(Hits, Start, End, FQuat::Identity, ECC_Visibility, FCollisionShape::MakeSphere(RadDamageParams.OuterRadius), TraceParams);

	RadDamageEvent.Origin = Hit.ImpactPoint;
	RadDamageEvent.Params = RadDamageParams;
	RadDamageEvent.ComponentHits = Hits;

	for (FHitResult nHit : Hits)
	{
		Hit.GetActor()->TakeDamage(RadDamageParams.BaseDamage, RadDamageEvent, Instigator->GetController(), this);
	}

}

void AProjectileBase::SpawnImpactEffect(FHitResult& Hit)
{
	FVector Location = Hit.ImpactPoint;
	FRotator Rotation = Hit.ImpactNormal.Rotation();
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Location, Rotation, true);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Location, Rotation, 1, 1, 0);
}

void AProjectileBase::Initialize(FVector Direction)
{
	Velocity = Direction * InitialSpeed;
}

void AProjectileBase::Move(float DeltaTime)
{
	SetActorLocation(GetActorLocation() + Velocity * DeltaTime);
}
