// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "GameDemo.h"
#include "MyPlayer.generated.h"

UCLASS()
class GAMEDEMO_API AMyPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// OVERRIDES
	AMyPlayer();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* Damage) override;

	// INPUTS
	void MoveForward(float val);
	void MoveRight(float val);
	void StartFire();
	void StopFire();
	void SwitchToRifle();
	void SwitchToLaser();
	void SwitchToRocket();

	// INTERACTIONS
	void OnDeath();
	void Heal(float HealAmount);
	void BeginSprint();
	void EndSprint();
	void AddAmmo(int32 AmmoAmount, EAmmoType AmmoType);
	void AddToInventory(class AWeaponBase* NewWeapon);
	void EquipWeapon(class AWeaponBase* WeaponToEquip);

	// HUD
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = HUD)
		void HUDUpdateHP();


protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* PlayerCamera;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		USpringArmComponent* CameraBoom;

private:
	UPROPERTY(EditDefaultsOnly)
		float MaxHealth = 100.f;

	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Health)
		float HealthPoints;

	UPROPERTY(EditDefaultsOnly)
		float WalkSpeed;
	UPROPERTY(EditDefaultsOnly)
		float SprintSpeed;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Hack)
	FPlayerInventory Inventory;
	UPROPERTY(EditDefaultsOnly)
		bool bEquipNewWeapon = false;
	UPROPERTY(EditDefaultsOnly)
		FName WeaponSocketName;
	
	bool bIsSprinting = false;

};
