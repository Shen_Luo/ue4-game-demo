// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#ifndef GAMEDEMO_GameDemoGameMode_generated_h
	#include "GameDemoGameMode.h"
#endif
#ifndef GAMEDEMO_GameDemo_generated_h
	#include "GameDemo.h"
#endif
#ifndef GAMEDEMO_InteractableActor_generated_h
	#include "InteractableActor.h"
#endif
#ifndef GAMEDEMO_MyPlayer_generated_h
	#include "MyPlayer.h"
#endif
#ifndef GAMEDEMO_PickUpBase_generated_h
	#include "PickUpBase.h"
#endif
#ifndef GAMEDEMO_AmmoPickUpBase_generated_h
	#include "AmmoPickUpBase.h"
#endif
#ifndef GAMEDEMO_MyHealthPickUp_generated_h
	#include "MyHealthPickUp.h"
#endif
#ifndef GAMEDEMO_WeaponBase_generated_h
	#include "WeaponBase.h"
#endif
#ifndef GAMEDEMO_LaserBase_generated_h
	#include "LaserBase.h"
#endif
#ifndef GAMEDEMO_RifleBase_generated_h
	#include "RifleBase.h"
#endif
#ifndef GAMEDEMO_RocketLauncherBase_generated_h
	#include "RocketLauncherBase.h"
#endif
#ifndef GAMEDEMO_ProjectileBase_generated_h
	#include "ProjectileBase.h"
#endif
