// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameDemo.h"
#include "PickUpBase.generated.h"

UCLASS()
class GAMEDEMO_API APickUpBase : public AActor
{
	GENERATED_BODY()
	
public:		
	APickUpBase();

	// OVERRIDES
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	// INTERACTIONS
	UFUNCTION(BlueprintNativeEvent, Category = Interaction)
	void OnInteract(AActor* Caller);
	virtual void OnInteract_Implementation(AActor* Caller);
	UFUNCTION()
	virtual void OnTriggerEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void SetCanInteract(bool NewInteract);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowSprintSpeedAccess = "true"))
		UStaticMeshComponent* PickUpMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowSprintSpeedAccess = "true"))
		USphereComponent* CollisionSphere;
	UPROPERTY(EditDefaultsOnly)
		bool bCanInteract;
private:
	UPROPERTY(EditDefaultsOnly)
	bool bTouchInteracts;
	
};
