// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "MyPlayer.h"
#include "Engine.h"
#include "LaserBase.h"
#include "RifleBase.h"
#include "RocketLauncherBase.h"

AMyPlayer::AMyPlayer()
{
	PrimaryActorTick.bCanEverTick = true;
	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));

	CameraBoom->AttachTo(RootComponent);
	PlayerCamera->AttachTo(CameraBoom);


}

void AMyPlayer::BeginPlay()
{
	Super::BeginPlay();

	HealthPoints = MaxHealth;
	HUDUpdateHP();
}


void AMyPlayer::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (Controller && Controller->IsLocalController())
	{
		
		// Character look to cursor
		APlayerController* PlayerController = Cast<APlayerController>(Controller);
		FVector MouseLocation, MouseDirection;
		PlayerController->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);
		SetActorRotation(FRotator(0, MouseDirection.Rotation().Yaw, 0));

	}
		

}


void AMyPlayer::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	// Bind movement axis
	InputComponent->BindAxis("MoveForward", this, &AMyPlayer::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyPlayer::MoveRight);
	
	// Bind sprint action
	InputComponent->BindAction("Sprint", IE_Pressed, this, &AMyPlayer::BeginSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AMyPlayer::EndSprint);

	// Bind fire action
	InputComponent->BindAction("Fire", IE_Pressed, this, &AMyPlayer::StartFire);
	InputComponent->BindAction("Fire", IE_Released, this, &AMyPlayer::StopFire);

	// Bind equip weapon action
	InputComponent->BindAction("SwitchToRifle", IE_Pressed, this, &AMyPlayer::SwitchToRifle);
	InputComponent->BindAction("SwitchToLaser", IE_Pressed, this, &AMyPlayer::SwitchToLaser);
	InputComponent->BindAction("SwitchToRocket", IE_Pressed, this, &AMyPlayer::SwitchToRocket);
}

void AMyPlayer::MoveForward(float val)
{
	FVector Forward(1, 0, 0);
	AddMovementInput(Forward, val);
}

void AMyPlayer::MoveRight(float val)
{
	FVector Right(0, 1, 0); 
	AddMovementInput(Right, val);
}

void AMyPlayer::StartFire()
{
	if (Inventory.CurrentWeapon)
	{
		Inventory.CurrentWeapon->StartFire();
	}
}

void AMyPlayer::StopFire()
{
	if (Inventory.CurrentWeapon)
	{
		Inventory.CurrentWeapon->StopFire();
	}
}

void AMyPlayer::SwitchToRifle()
{
	if (Inventory.Rifle)
		EquipWeapon(Inventory.Rifle);
}

void AMyPlayer::SwitchToLaser()
{
	if (Inventory.Laser)
		EquipWeapon(Inventory.Laser);
}

void AMyPlayer::SwitchToRocket()
{
	if (Inventory.RocketLauncher)
		EquipWeapon(Inventory.RocketLauncher);
}

void AMyPlayer::BeginSprint()
{
	bIsSprinting = true;
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
}

void AMyPlayer::EndSprint()
{
	bIsSprinting = false;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}


float AMyPlayer::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* Damage)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, Damage);
	HealthPoints -= ActualDamage;
	HUDUpdateHP();
	if (HealthPoints <= 0)
		OnDeath();
	return ActualDamage;
}

void AMyPlayer::OnDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("You have died!"));
	Destroy();

}

void AMyPlayer::Heal(float HealAmount)
{
	if(HealthPoints < MaxHealth && HealAmount > 0)
	HealthPoints += HealAmount;
	HUDUpdateHP();
}

void AMyPlayer::AddAmmo(int32 AmmoAmount, EAmmoType AmmoType)
{
	switch (AmmoType)
	{
		case EAmmoType::AT_Bullet:
			if (Inventory.Rifle)
			{
				Inventory.Rifle->AddAmmo(AmmoAmount);
			}
			break;
		case EAmmoType::AT_Laser:
			if (Inventory.Laser)
			{
				Inventory.Laser->AddAmmo(AmmoAmount);
			}
			break;
		case EAmmoType::AT_Rocket:
			if (Inventory.RocketLauncher)
			{
				Inventory.RocketLauncher->AddAmmo(AmmoAmount);
			}
			break;
	}
}

void AMyPlayer::AddToInventory(class AWeaponBase* NewWeapon)
{
	NewWeapon->SetCanInteract(false);
	NewWeapon->SetActorEnableCollision(false);
	NewWeapon->ChangeOwner(this);
	NewWeapon->AttachRootComponentTo(GetMesh(), WeaponSocketName, EAttachLocation::SnapToTarget);
	NewWeapon->SetActorHiddenInGame(true);

	if (NewWeapon->IsA(ARifleBase::StaticClass()))
	{
		if (Inventory.Rifle)
		{
			Inventory.Rifle->Destroy();
		}
		Inventory.Rifle = Cast<ARifleBase>(NewWeapon);
		if (!Inventory.CurrentWeapon || bEquipNewWeapon)
			EquipWeapon(Inventory.Rifle);
	}
	else if (NewWeapon->IsA(ALaserBase::StaticClass()))
	{
		if (Inventory.Laser)
		{
			Inventory.Laser->Destroy();
		}
		Inventory.Laser = Cast<ALaserBase>(NewWeapon);
		if (!Inventory.CurrentWeapon || bEquipNewWeapon)
			EquipWeapon(Inventory.Laser);
	}
	else if (NewWeapon->IsA(ARocketLauncherBase::StaticClass()))
	{
		if (Inventory.RocketLauncher)
		{
			Inventory.Laser->Destroy();
		}
		Inventory.RocketLauncher = Cast<ARocketLauncherBase>(NewWeapon);
		if (!Inventory.CurrentWeapon || bEquipNewWeapon)
			EquipWeapon(Inventory.RocketLauncher);
	}
}

void AMyPlayer::EquipWeapon(class AWeaponBase* WeaponToEquip)
{
	if (WeaponToEquip == Inventory.CurrentWeapon)
		return;

	if (Inventory.CurrentWeapon)
	{
		Inventory.CurrentWeapon->SetActorHiddenInGame(true);
	}

	if (WeaponToEquip == Inventory.Rifle)
	{
		Inventory.CurrentWeapon = Inventory.Rifle;
	}
	else if (WeaponToEquip == Inventory.Laser)
	{
		Inventory.CurrentWeapon = Inventory.Laser;
	}
	else if (WeaponToEquip == Inventory.RocketLauncher)
	{
		Inventory.CurrentWeapon = Inventory.RocketLauncher;
	}

	Inventory.CurrentWeapon->SetActorHiddenInGame(false);
}

