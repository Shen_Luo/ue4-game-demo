// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PickUpBase.h"
#include "MyHealthPickUp.generated.h"

/**
 * 
 */
UCLASS()
class GAMEDEMO_API AMyHealthPickUp : public APickUpBase
{
	GENERATED_BODY()
	
public: 
	virtual void OnInteract_Implementation(AActor* Caller) override;

	

private:
	UPROPERTY(EditDefaultsOnly)
	float HealAmount = 25.f;
	
		

	
	
};
