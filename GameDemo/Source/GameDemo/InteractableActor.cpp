// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "MyPlayer.h"
#include "InteractableActor.h"

AInteractableActor::AInteractableActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AInteractableActor::BeginPlay()
{
	Super::BeginPlay();	

	//find all the meshes on the actor on begin play
	for (UActorComponent* Mesh : GetComponentsByClass(UMeshComponent::StaticClass()))
	{
		UMeshComponent* thisMesh = Cast<UMeshComponent>(Mesh);
		if (thisMesh)
		{
			Meshs.Push(thisMesh);
		}
	}
}

void AInteractableActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void AInteractableActor::GetPlayer(AActor* Caller)
{
	Player = Cast<AMyPlayer>(Caller);
	if (Player)
	{
		OnInteract_Implementation(Player);
	}
}

void AInteractableActor::OnInteract_Implementation(AActor* Caller)
{
	Destroy();
}


void AInteractableActor::OnBeginFocus()
{
	if(bCanInteract)
	for (UMeshComponent* Mesh : Meshs)
	{
		Mesh->SetRenderCustomDepth(true);
		Mesh->SetCustomDepthStencilValue((uint8)Color);
	}
}

void AInteractableActor::OnEndFocus()
{
	for (UMeshComponent* Mesh : Meshs)
	{
		Mesh->SetRenderCustomDepth(false);
		
	}
}


