// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "PickUpBase.h"



APickUpBase::APickUpBase()
{
 
	PrimaryActorTick.bCanEverTick = true;
	
	PickUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pick Up Mesh"));
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	RootComponent = CollisionSphere;
	//set sphere collision to overlap all
	CollisionSphere->SetCollisionProfileName("OverlapAllDynamic");
	PickUpMesh->AttachTo(RootComponent);
	//set mesh collision to none
	PickUpMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	//set up overlap event delegate
	//CollisionSphere->bGenerateOverlapEvents(true);
	
}

void APickUpBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickUpBase::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (bTouchInteracts)
	{
		CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUpBase::OnTriggerEnter);
	}

}

void APickUpBase::OnInteract_Implementation(AActor* Caller)
{

}

void APickUpBase::OnTriggerEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bCanInteract)
	{
		OnInteract(OtherActor);
	}
	
}

void APickUpBase::SetCanInteract(bool NewInteract)
{
	bCanInteract = NewInteract;
}