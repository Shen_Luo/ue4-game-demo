// Fill out your copyright notice in the Description page of Project Settings.

#include "GameDemo.h"
#include "LaserBase.h"
#include "Engine.h"

void ALaserBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!bIsFiring && !HasFullAmmo())
	{
		
		Charge += RechargeAmount * DeltaTime;
		if (Charge > 1)
		{
			AddAmmo((int32)Charge);
			Charge--;
		}
	}
}


