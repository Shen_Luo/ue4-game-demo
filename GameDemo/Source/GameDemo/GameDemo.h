// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define ECC_Weapon ECC_GameTraceChannel1;

#include "Engine.h"
#include "GameDemo.generated.h"

UENUM(BlueprintType)
enum class EStencilColor : uint8
{
	SC_Green = 252 UMETA(DisplayName = "Green"),
	SC_Blue = 253 UMETA(DisplayName = "Blue"),
	SC_Red = 254 UMETA(DisplayName = "Red")
};

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	AT_Bullet UMETA(DisplayName = "Bullet"),
	AT_Rocket UMETA(DisplayName = "Rocket"),
	AT_Laser UMETA(DisplayName = "Laser")

};

USTRUCT(BlueprintType)
struct FPlayerInventory
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerCamera)
	class AWeaponBase* CurrentWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerCamera)
	class ARifleBase* Rifle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerCamera)
	class ALaserBase* Laser;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerCamera)
	class ARocketLauncherBase* RocketLauncher;

};